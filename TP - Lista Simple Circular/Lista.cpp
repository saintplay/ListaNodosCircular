#include "Lista.h"
#include "Windows.h"

using namespace std;

Lista::Lista() {
	inicio = nullptr;
}

Lista::~Lista() {
	if (inicio != nullptr) {
		delete inicio;
	}
}

void Lista::setInicio(Nodo* inicio) {
	this->inicio = inicio;
}

Nodo* Lista::getInicio() {
	return inicio;
}

void Lista::agregarDato(int dato) {
	Nodo* nuevo_nodo = new Nodo(dato);

	if (inicio == nullptr) {
		inicio = nuevo_nodo;
		nuevo_nodo->setSiguiente(inicio);
	}

	Nodo* nodo = inicio;

	while (nodo->getSiguiente() != inicio) {
		nodo = nodo->getSiguiente();
	}

	nodo->setSiguiente(nuevo_nodo);
	nuevo_nodo->setSiguiente(inicio);
}

void Lista::eliminarNodo(int posicion) {
	if (inicio == nullptr) {
		cout << "No hay ningun nodo" << endl;
		return;
	}

	if (inicio == inicio->getSiguiente()) {
		inicio = nullptr;
		cout << "Nodo Borrado. Ahora ya no queda ningun nodo." << endl;
		return;
	}

	if (posicion == 0) {
		Nodo* anterior = inicio;
		Nodo* nodo = inicio->getSiguiente();

		while (nodo != inicio) {
			anterior = nodo;
			nodo = nodo->getSiguiente();
		}

		anterior->setSiguiente(nodo->getSiguiente());
		inicio = anterior->getSiguiente();

		cout << "Nodo Borrado." << endl;
		return;
	}

	Nodo* nodo = inicio->getSiguiente();
	Nodo* anterior = inicio;

	int index = 1;
	
	while (posicion != index) {
		anterior = nodo;
		nodo = nodo->getSiguiente();
		index++;

		if (posicion > index) {
			cout << "Nodo se encontro dicha posicion." << endl;
			return;
		}
	}

	anterior->setSiguiente(nodo->getSiguiente());
	cout << "Nodo Borrado." << endl;
}

void Lista::mostrarDatos() {
	
	if (inicio == nullptr) {
		cout << "No hay ningun nodo" << endl;
		return;
	}
	
	Nodo* nodo = inicio;
	cout << endl;

	cout << nodo->getDato();
	nodo = nodo->getSiguiente();

	while (nodo != inicio) {
		cout << " -> " << nodo->getDato();
		nodo = nodo->getSiguiente();
	}

	//Obtener la posicion del cursor
	CONSOLE_SCREEN_BUFFER_INFO cursor;
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(handle, &cursor);

	cout << " " << char(196) << char(191) << endl;

	cout << "\030";

	for (int i = 0; i <= cursor.dwCursorPosition.X; i++) {
		cout << " ";
	}
	cout << char(179) << endl;

	cout << char(192);

	for (int i = 0; i <= cursor.dwCursorPosition.X; i++) {
		cout << char(196);
	}

	cout << char(217) << endl;

	cout << endl;
}