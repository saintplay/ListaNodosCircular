#include "Nodo.h"
#include <iostream>

class Lista
{
public:
	Lista();
	~Lista();

	void setInicio(Nodo* inicio);
	Nodo* getInicio();

	//Agregar al final(Una posicion antes del inicio)
	void agregarDato(int dato);
	void eliminarNodo(int posicion);

	void mostrarDatos();

private:
	Nodo* inicio;
};