class Nodo
{
public:
	Nodo();
	Nodo(int dato);
	~Nodo();

	void setDato(int dato);
	void setSiguiente(Nodo* siguiente);

	int getDato();
	Nodo* getSiguiente();

private:
	int dato;
	Nodo* siguiente;
};