#include "Nodo.h"

Nodo::Nodo() {
	siguiente = nullptr;
}

Nodo::Nodo(int dato) {
	siguiente = nullptr;
	this->dato = dato;
}

Nodo::~Nodo() {
	if (siguiente != nullptr) {
		delete siguiente;
	}
}

void Nodo::setDato(int dato) {
	this->dato = dato;
}

void Nodo::setSiguiente(Nodo* siguiente) {
	this->siguiente = siguiente;
}

int Nodo::getDato() {
	return dato;
}

Nodo* Nodo::getSiguiente() {
	return siguiente;
}