#include "Lista.h"

using namespace std;

Lista* lista;
bool ordenado;

void agregarDato() {
	int nuevo_dato;

	cout << "Ingrese nuevo dato: ";
	cin >> nuevo_dato;

	lista->agregarDato(nuevo_dato);
	cout << "Elemento agregado!" << endl;
}

void eliminar() {
	int posicion;

	cout << "Ingrese la posicion que quiere eliminar: ";
	cin >> posicion;

	lista->eliminarNodo(posicion);
}


void mostrarMenu() {
	char opcion[1];
	opcion[0] = 'a';

	while (opcion[0] != '4') {
		cout << "1: Agregar" << endl;
		cout << "2: Eliminar" << endl;
		cout << "3: Mostrar" << endl;
		cout << "4: Salir" << endl;
		cout << "Ingrese su opcion: ";
		cin >> opcion;

		switch (opcion[0]) {
		case '1':
			agregarDato();
			break;
		case '2':
			eliminar();
			break;
		case '3':
			lista->mostrarDatos();
			break;
		case '4':
			;
		default:
			cout << "Ingrese una opcion valida" << endl;

		}
	}
}

int main() {
		
	lista = new Lista();

	mostrarMenu();
	return 0;
}